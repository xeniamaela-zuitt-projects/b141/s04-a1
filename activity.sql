a. Find all the artists that has letter D in its name.
------------------------------------------------------------------------------------------------
SELECT name FROM artists WHERE name LIKE "%d%";

b. Find all songs that has a length of less than 230.
------------------------------------------------------------------------------------------------
SELECT * FROM songs WHERE length < 230;

c. Join the 'albums' and 'songs' tables. (Only show the album name, song name, and song length.)
------------------------------------------------------------------------------------------------
SELECT albums.name AS albums_name, songs.title AS songs_title, length
FROM albums JOIN songs ON albums.id = songs.album_id;

d. Join the 'artists' and 'albums' tables. (Find all albums that has letter A in its name.)
------------------------------------------------------------------------------------------------
SELECT artists.name AS artists_name, albums.name AS albums_name
FROM artists JOIN albums ON artists.id = albums.artist_id
WHERE albums.name LIKE "%a%";

e. Sort the albums in Z-A order. (Show only the first 4 records.)
------------------------------------------------------------------------------------------------
SELECT name FROM albums ORDER BY name DESC LIMIT 4;

f. Join the 'albums' and 'songs' tables. (Sort albums from Z-A and sort songs from A-Z.)
------------------------------------------------------------------------------------------------
SELECT albums.name AS albums_name, songs.title AS songs_title
FROM albums JOIN songs ON albums.id = songs.album_id
ORDER BY albums.name DESC;

SELECT albums.name AS albums_name, songs.title AS songs_title
FROM albums JOIN songs ON albums.id = songs.album_id
ORDER BY songs.title ASC;
